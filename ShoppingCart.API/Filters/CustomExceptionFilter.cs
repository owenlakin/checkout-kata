﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.Filters;
using System;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Models;
using Models.Enum;

namespace Projects.API.Filters
{
    public class CustomExceptionFilter : IExceptionFilter
    {
        private readonly string APIKey;

        public CustomExceptionFilter(IConfiguration config)
        {
            APIKey = config.GetSection("APIKey").Value;
        }

        public void OnException(ExceptionContext context)
        {
            HttpResponse response = context.HttpContext.Response;
            response.ContentType = "application/json";
            response.StatusCode = StatusCodes.Status500InternalServerError;
            context.Result = new BadRequestObjectResult(new APIResponse() { Message = context.Exception.Message, ResponseStatusCode = ResponseStatusCode.Error, Data = null });
        }
    }
}