﻿namespace Models.Config
{
    public class AppSettings
    {
        public string APIKey { get; set; }
        public double StandardMarkup { get; set; }
        public string SecretAPIKey { get; set; }
    }
}
