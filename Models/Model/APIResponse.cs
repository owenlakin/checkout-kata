﻿using Models.Enum;

namespace Models
{
    public class APIResponse
    {
        public ResponseStatusCode ResponseStatusCode { get; set; }
        public string Message { get; set; }
        public object Data { get; set; }
    }
}
